# BookStack Backup Script

This is a simple BookStack backup script, to dump the database, copy uploaded files, and zip it all up into a timestamped archive. 
This is designed for an on-system install, not a docker setup.
Database credentails are automatically read from your BookStack config.

This script will copy uploads before zipping, so you'll need more free space on your system than your BookStack directory already consumes. 

### Usage

1. Copy the script down to a file (`bookstack-backup.sh`).
2. Tweak the configu variables at the top of the script.
3. Make the script executable (`chmod +x bookstack-backup.sh`).
4. Run the script (`./bookstack-backup.sh`).

